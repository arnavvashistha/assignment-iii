import { Component, OnInit } from '@angular/core';
import{Params,ActivatedRoute} from '@angular/router';
import {Location} from '@angular/common';
import { Dish } from '../shared/dish';
import {Comment,RATINGS} from '../shared/comment';
import {FormGroup,FormBuilder,Validators} from '@angular/forms';
import {DishService} from '../services/dish.service';
import 'rxjs/add/operator/switchMap';


@Component({
  selector: 'app-dishdetail',
  templateUrl: './dishdetail.component.html',
  styleUrls: ['./dishdetail.component.css']
})


export class DishdetailComponent implements OnInit {
  dish: Dish;
  dishIds:number[];
  prev:number;
  next:number;
  commentForm:FormGroup;
  comment:Comment;
  ratings=RATINGS;

  constructor(private dishservice : DishService , private location : Location,private route : ActivatedRoute, private fb : FormBuilder) {
  this.createAddCommentsFrom(); }

  ngOnInit() {
    this.dishservice.getDishIds()
    .subscribe(dishIds=>this.dishIds=dishIds);
    this.route.params
    .switchMap((params:Params)=>this.dishservice.getDish(+params['id']))
    .subscribe(dish => {
      this.dish=dish;
      this.setPrevNext(dish.id);
    });
  };
  commentFormErrors = {
    'rating': '',
    'author': '',
    'comment': ''
  };

  commentFormValidationMessages = {
    'rating': {
      'required':      'Rating is required.'
    },
    'author': {
      'required':      'Author Name is required.',
      'minlength':     'Authot Name must be at least 2 characters long.',
      'maxlength':     'Author Name cannot be more than 25 characters long.'
    },
    'comment': {
      'required':      'Comment is required.'
    }
  };

  setPrevNext(dishId:number){
    let index = this.dishIds.indexOf(dishId);
    this.prev = this.dishIds[(this.dishIds.length + index - 1)%this.dishIds.length];
    this.next = this.dishIds[(this.dishIds.length + index + 1)%this.dishIds.length];
  };
  goBack():void{
    this.location.back();
  };
  createAddCommentsFrom():void{
    this.commentForm = this.fb.group({
      rating: [5,Validators.required],
      author: ['',[Validators.required,Validators.minLength(2),Validators.maxLength(25)]],
      comment: ['',Validators.required]
    });
    this.commentForm.valueChanges
      .subscribe(data => this.onValueChanged(data));

    this.onValueChanged(); // (re)set validation messages now
  };
  onValueChanged(data?: any) {
    if (!this.commentForm) { return; }
    const form = this.commentForm;
    for (const field in this.commentFormErrors) {
      // clear previous error message (if any)
      this.commentFormErrors[field] = '';
      const control = form.get(field);
      if (control && control.dirty && !control.valid) {
        const messages = this.commentFormValidationMessages[field];
        for (const key in control.errors) {
          this.commentFormErrors[field] += messages[key] + ' ';
        }
      }
    }
  };

  onSubmit(){
    this.comment = this.commentForm.value;
    console.log(this.comment);
    let currentDate = new Date();
    this.comment.date = currentDate.toString();
    console.log(this.comment);
    this.dish.comments.push(this.comment);
    this.commentForm.reset({
      rating: 5,
      author: '',
      Comment: ''
    });
  }

}
