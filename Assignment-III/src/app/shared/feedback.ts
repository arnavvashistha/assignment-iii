class Feedback {
    firstname: string;
    lastname: string;
    telnum: number;
    email: string;
    agree: boolean;
    contacttype: string;
    message: string;
};

 const ContactType = ['None', 'Tel', 'Email'];

export {Feedback, ContactType};
